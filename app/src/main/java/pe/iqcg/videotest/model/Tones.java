package pe.iqcg.videotest.model;

/**
 * Created by jprada on 04/10/2017.
 */

class Tones {
	private String _0;
	private String _p1;
	private String _p2;
	private String _m1;
	private String _m2;

	public String get_0() {
		return _0;
	}

	public void set_0(String _0) {
		this._0 = _0;
	}

	public String get_p1() {
		return _p1;
	}

	public void set_p1(String _p1) {
		this._p1 = _p1;
	}

	public String get_p2() {
		return _p2;
	}

	public void set_p2(String _p2) {
		this._p2 = _p2;
	}

	public String get_m1() {
		return _m1;
	}

	public void set_m1(String _m1) {
		this._m1 = _m1;
	}

	public String get_m2() {
		return _m2;
	}

	public void set_m2(String _m2) {
		this._m2 = _m2;
	}
}
