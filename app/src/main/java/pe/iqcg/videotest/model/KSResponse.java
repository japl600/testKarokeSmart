package pe.iqcg.videotest.model;

/**
 * Created by jprada on 05/10/2017.
 */

public class KSResponse {
	private KSData data;
	private String success;

	public KSData getData() {
		return data;
	}

	public void setData(KSData data) {
		this.data = data;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}
}
