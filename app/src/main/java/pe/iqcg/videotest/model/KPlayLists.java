package pe.iqcg.videotest.model;

/**
 * Created by jprada on 04/10/2017.
 */

class KPlayLists {
	private String name;
	private String img;
	private int id;
	private int total_songs;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotal_songs() {
		return total_songs;
	}

	public void setTotal_songs(int total_songs) {
		this.total_songs = total_songs;
	}
}
