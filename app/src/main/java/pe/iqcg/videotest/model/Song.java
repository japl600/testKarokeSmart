package pe.iqcg.videotest.model;

/**
 * Created by jprada on 04/10/2017.
 */

public class Song {
	private String video_buffer;
	private String tag;
	private String video;
	private String name;
	private Tones tones;
	private String artist;
	private int id;
	private int like;

	public String getVideo_buffer() {
		return video_buffer;
	}

	public void setVideo_buffer(String video_buffer) {
		this.video_buffer = video_buffer;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Tones getTones() {
		return tones;
	}

	public void setTones(Tones tones) {
		this.tones = tones;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLike() {
		return like;
	}

	public void setLike(int like) {
		this.like = like;
	}
}
