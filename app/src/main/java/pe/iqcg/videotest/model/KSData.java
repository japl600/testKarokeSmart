package pe.iqcg.videotest.model;

import java.util.ArrayList;

/**
 * Created by jprada on 04/10/2017.
 */

public class KSData {
	private SongDay song_day;
	private ArrayList<PlayListGroups> playlist_groups;

	public SongDay getSongDay() {
		return song_day;
	}

	public void setSongDay(SongDay songDay) {
		this.song_day = songDay;
	}

	public ArrayList<PlayListGroups> getGroups() {
		return playlist_groups;
	}

	public void setGroups(ArrayList<PlayListGroups> groups) {
		this.playlist_groups = groups;
	}
}
