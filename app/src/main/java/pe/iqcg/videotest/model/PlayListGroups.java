package pe.iqcg.videotest.model;

import java.util.ArrayList;

/**
 * Created by jprada on 04/10/2017.
 */

class PlayListGroups {
	private String name;
	private String img;
	private ArrayList<KPlayLists> playlists;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public ArrayList<KPlayLists> getPlayLists() {
		return playlists;
	}

	public void setPlayLists(ArrayList<KPlayLists> playLists) {
		this.playlists = playLists;
	}
}
