package pe.iqcg.videotest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by jprada on 29/09/2017.
 */

public class MediaPlayer extends Activity {

	private VideoView myVideoView;
	private int position = 0;
	private ProgressDialog progressDialog;
	private MediaController mediaControls;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the main layout of the activity
		setContentView(R.layout.activity_audio_recorder);

		//set the media controller buttons
		if (mediaControls == null) {
			mediaControls = new MediaController(MediaPlayer.this);
		}

		//initialize the VideoView
		myVideoView = (VideoView) findViewById(R.id.video_view);

		// create a progress bar while the video file is loading
		progressDialog = new ProgressDialog(MediaPlayer.this);
		// set a title for the progress bar
		progressDialog.setTitle("JavaCodeGeeks Android Video View Example");
		// set a message for the progress bar
		progressDialog.setMessage("Loading...");
		//set the progress bar not cancelable on users' touch
		progressDialog.setCancelable(false);
		// show the progress bar
		progressDialog.show();

		try {
			//set the media controller in the VideoView
			myVideoView.setMediaController(mediaControls);

			//set the uri of the video to be played
			myVideoView.setVideoURI(Uri.parse("https://kspitch.blob.core.windows.net/asset-7d5fef2d-76aa-4c1a-ba34-45e9abb73751/ChhSDAZLAT_v0.mp4?sv=2012-02-12&sr=c&si=637ecbb3-ef5e-46cf-99ed-1ec10ed2772d&sig=15za6lfNb68e9m5Zqa%2BhEAdXhKtgW9BgQIdc2ocTIcw%3D&se=2025-02-02T04:44:42Z"));

		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}

		myVideoView.requestFocus();
		//we also set an setOnPreparedListener in order to know when the video file is ready for playback
		myVideoView.setOnPreparedListener(new android.media.MediaPlayer.OnPreparedListener() {

			public void onPrepared(android.media.MediaPlayer mediaPlayer) {
				// close the progress bar and play the video
				progressDialog.dismiss();
				//if we have a position on savedInstanceState, the video playback should start from here
				myVideoView.seekTo(position);
				if (position == 0) {
					myVideoView.start();
				} else {
					//if we come from a resumed activity, video playback will be paused
					myVideoView.pause();
				}
			}
		});

	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		//we use onSaveInstanceState in order to store the video playback position for orientation change
		savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
		myVideoView.pause();
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		//we use onRestoreInstanceState in order to play the video playback from the stored position
		position = savedInstanceState.getInt("Position");
		myVideoView.seekTo(position);
	}
}
