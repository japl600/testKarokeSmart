package pe.iqcg.videotest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import pe.iqcg.videotest.databinding.ActivityMainBinding;
import pe.iqcg.videotest.model.KSData;
import pe.iqcg.videotest.model.KSResponse;
import pe.iqcg.videotest.model.Song;
import pe.iqcg.videotest.model.SongDay;
import pe.iqcg.videotest.threads.DownloadImageTask;
import pe.iqcg.videotest.ws.HelperWS;
import pe.iqcg.videotest.ws.MetodosWS;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

public class MainActivity extends Activity implements View.OnClickListener {

	private final String TAG = getClass().getSimpleName();
	private ActivityMainBinding binding;
	private ProgressDialog progressDialog;
	private Context mContext;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = MainActivity.this;
		binding= DataBindingUtil.setContentView(this,R.layout.activity_main);

		getData();

	}

	private void getData() {
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage("Cargando...");
		progressDialog.setCancelable(false);
		progressDialog.show();
		try {

			MetodosWS metodosWS = HelperWS.getConfiguration().create(MetodosWS.class);
			Call<KSResponse> responseCall = metodosWS.getData("PE");
			responseCall.enqueue(new Callback<KSResponse>() {
				@Override
				public void onResponse(Call<KSResponse> call, Response<KSResponse> response) {
					KSResponse result = response.body();
					if (result != null) {
						KSData data = result.getData();
						SongDay songDay = data.getSongDay();
						Song song = songDay.getSong();

						//load image
						new DownloadImageTask((ImageView)binding.ivSong).execute(songDay.getImg());
						//set texts
						binding.tvSongName.setText(song.getName());
						binding.tvSongArtist.setText(song.getArtist());

					}

					progressDialog.dismiss();
				}

				@Override
				public void onFailure(Call<KSResponse> call, Throwable t) {
					progressDialog.dismiss();
					Log.w(TAG, "Something go wrong");
				}
			});
		}catch (Exception e){
			e.printStackTrace();
			progressDialog.dismiss();
		}

	}


	@Override
	public void onClick(View v) {

	}
}