package pe.iqcg.videotest.ws;

import pe.iqcg.videotest.model.KSResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jprada on 05/10/2017.
 */

public interface MetodosWS {

	@GET("playlist_groups")
	Call<KSResponse> getData(@Query("country") String pais);

}
